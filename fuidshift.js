const DIR = "rootfs";
const UID_SHIFT = 441248;
const GID_SHIFT = 441248;
var fs = require("fs");
var path = require("path");

(function recurseDirectory(dirpath){
    let ls = fs.readdirSync(dirpath);
    for (let itemname of ls) {
        let itempath = path.join(dirpath, itemname);
        console.log(itempath);
        let itemstat = fs.lstatSync(itempath);
        if (!itemstat.isSymbolicLink()) fs.chownSync(itempath, itemstat.uid + UID_SHIFT, itemstat.gid + GID_SHIFT);
        if (itemstat.isDirectory()) recurseDirectory(itempath);
    }
})(DIR);